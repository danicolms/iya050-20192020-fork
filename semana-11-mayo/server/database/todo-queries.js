const fetch = require('node-fetch')

async function all() {
  const response = await fetch('http://localhost:6000', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      action: 'read-all',
    })
  })

  const body = await response.json()

  return body
}

async function get(id) {
  const response = await fetch('http://localhost:6000', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      action: 'read',
      id
    })
  })

  const body = await response.json()
  
  return body
}

async function create(title, order) {
  await fetch('http://localhost:6000', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      action: 'create',
      id: (10000000000 * Math.random()).toFixed(),
      item: {
        title,
        order
      }
    })
  })

  return {
    id,
    title,
    order
  }
}

async function update(id, properties) {
  await fetch('http://localhost:6000', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      action: 'update',
      id,
      item: properties
    })
  })

  return {
    id,
    ...properties
  }
}

// delete is a reserved keyword
async function del(id) {
  await fetch('http://localhost:6000', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      action: 'update',
      id
    })
  })

  return {
    id,
    ...properties
  }
}

async function clear() {
  const response = await fetch('http://localhost:6000', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      action: 'clear'
    })
  })

  const body = await response.json()

  return body
}

module.exports = {
    all,
    get,
    create,
    update,
    delete: del,
    clear
}
