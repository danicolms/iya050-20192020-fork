const { React, ReactDOM } = window;
const { PropTypes } = window;
const {
  BrowserRouter,
  Switch,
  Route,
  useHistory,
  useParams
} = window.ReactRouterDOM;

const newsStub = [
  {
    id: 0,
    image:
      "https://images.pexels.com/photos/3683056/pexels-photo-3683056.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title: "Los test rápidos de coronavirus incrementarán el número de casos",
    author: "LAURA G. IBAÑES",
    date: "18 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "Las críticas a la gestión de la epidemia de coronavirus se están extendiendo",
    seen: false
  },
  {
    id: 1,
    image:
      "https://images.pexels.com/photos/3856635/pexels-photo-3856635.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title:
      "La 'Terminator del ébola' que probará en humanos la vacuna del coronavirus",
    author: "LUCAS DE LA CAL",
    date: "18 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "Un equipo liderado por China ha desarrollado una vacuna contra el Covid-19 que va a probarse en humanos",
    seen: false
  },
  {
    id: 2,
    image:
      "https://images.pexels.com/photos/3431494/pexels-photo-3431494.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title:
      "Donald Trump anuncia el cierre temporal de la frontera con Canadá para combatir el coronavirus",
    author: "EFE",
    date: "18 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "El país también ha prohibido la entrada de personas desde Europa, China y otras regiones afectadas por la pandemia de coronavirus",
    seen: false
  },
  {
    id: 3,
    image:
      "https://images.pexels.com/photos/3748035/pexels-photo-3748035.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title:
      "Cancelado el Festival de Eurovisión a causa de la epidemia de Covid-19",
    author: "FÁTIMA ELIDRISSI",
    date: "18 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "Blas Cantó repetirá como representante español en 2021 del certamen que se ha celebrado sin interrupciones desde 1956",
    seen: false
  },
  {
    id: 4,
    image:
      "https://images.pexels.com/photos/1424246/pexels-photo-1424246.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title: "Los jugadores paran la Superliga de Turquía",
    author: "DIEGO TORRES",
    date: "19 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "La oposición de los futbolistas, temerosos del coronavirus, frena el empeño de la federación de mantener el campeonato local como la única liga activa de los países de la UEFA",
    seen: false
  },
  {
    id: 5,
    image:
      "https://images.pexels.com/photos/3560136/pexels-photo-3560136.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title:
      "Netflix acepta rebajar la calidad de sus emisiones para no colapsar Internet",
    author: "ÁLVARO SÁNCHEZ",
    date: "19 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "Netflix ha anunciado este jueves que reducirá la resolución a la que emite sus contenidos en Europa durante 30 días para evitar así la congestión de Internet",
    seen: false
  },
  {
    id: 6,
    image:
      "https://images.pexels.com/photos/2228561/pexels-photo-2228561.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=1280&w=1920",
    title: "El cambio de velocidades que impone el coronavirus",
    author: "BRENDA LOZANO",
    date: "18 marzo 2020",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie justo eu convallis vulputate. Morbi eleifend ligula pellentesque odio vulputate, nec facilisis orci fermentum. Integer ante risus, vehicula ac elementum vel, fermentum eu nisi. Etiam quis libero ex. Curabitur iaculis viverra diam in posuere. Nullam in venenatis tortor. Donec auctor, dui et euismod posuere, ligula turpis molestie felis, eget malesuada nisl dolor a metus. Suspendisse sapien purus, vehicula non mauris at, consectetur consectetur elit. Mauris porttitor gravida velit, sit amet vestibulum mi viverra sit amet. Morbi ante ante, volutpat ac rutrum sed, malesuada vel velit. Curabitur malesuada lorem eget blandit suscipit. Aenean sodales lectus sed nibh tincidunt rhoncus. Donec vestibulum enim quis purus ornare rutrum.

Suspendisse dignissim turpis id scelerisque mollis. Mauris nisl lorem, interdum quis feugiat vel, interdum efficitur lectus. Integer est lorem, feugiat a dapibus facilisis, iaculis id augue. Quisque non felis et ex malesuada scelerisque. Integer interdum aliquam imperdiet. In elit metus, tristique vel ante non, elementum lobortis metus. Nullam nibh urna, varius eu metus at, laoreet pellentesque eros.

Nulla libero ligula, dapibus ut ante et, pulvinar rhoncus augue. In suscipit euismod vehicula. Ut mollis orci ac porta vehicula. Vestibulum faucibus augue magna. Phasellus accumsan fermentum massa sit amet ornare. Sed ligula nisl, molestie ut augue in, faucibus interdum velit. Nulla in arcu sem. In ipsum neque, lacinia at tristique vitae, consectetur at metus. Fusce placerat elit turpis, vel bibendum dui egestas a. Morbi posuere suscipit neque in dapibus. Donec ullamcorper cursus felis ut condimentum.

Nulla congue ex magna, nec cursus nisl gravida eleifend. In efficitur ut sem eget posuere. Suspendisse consectetur varius vestibulum. Sed pellentesque feugiat purus sed tristique. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam id nibh neque. Integer nec sapien eget diam hendrerit placerat ac eget lectus. Aenean mattis at quam a congue. Duis in neque urna. Cras eget cursus ex. Sed viverra mauris sem. Curabitur vulputate quam vitae tristique sagittis. Quisque tincidunt ultricies luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Vestibulum sed mattis enim. Aliquam elementum tortor nulla, non dictum tellus pretium non. Sed vel quam condimentum, finibus orci eget, venenatis purus. Quisque velit ipsum, ullamcorper sit amet ornare in, convallis eget ante. Nunc porta sagittis pulvinar. Phasellus ut neque augue. Praesent lacinia erat eu risus efficitur efficitur. In scelerisque eu risus quis volutpat. Maecenas eu purus luctus, porta nisl at, rhoncus justo. Phasellus elit ipsum, efficitur eu commodo vel, condimentum a nulla.`,
    short:
      "La pandemia nos ha traído dos velocidades: la de la vida pública, nunca antes en la historia tan veloz para la ciencia, y la de la vida privada, nunca antes tan lenta",
    seen: false
  }
];

/* ==========================================================================
    Dashboard
   ========================================================================== */

const Dashboard = () => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);

  const handleModalOpen = state => {
    setIsModalOpen(state);
  };

  return (
    <div className="dashboard__wrapper">
      <BrowserRouter>
        <Header isModalOpen={handleModalOpen} />
        <Switch>
          <Route path="/news/:id">
            {" "}
            <Article />{" "}
          </Route>
          <Route path="/">
            {isModalOpen && <Add isModalOpen={handleModalOpen}></Add>}
            <Home />
          </Route>
        </Switch>{" "}
      </BrowserRouter>
    </div>
  );
};

/* Header
  ========================================================================== */

const Header = props => {
  let history = useHistory();

  function handleAddClick() {
    props.isModalOpen(true);
  }
  return (
    <div className="header__wrapper">
      <p className="header__logo">
        <div
          onClick={() => {
            history.push("/");
          }}
        >
          DailyNews
        </div>
      </p>
      {history.location.pathname === "/" && (
        <div className="header__action" onClick={handleAddClick}>
          <span> AGREGAR+ </span>
        </div>
      )}
    </div>
  );
};

Header.propTypes = {
  isModalOpen: PropTypes.function
};
/* Home
  ========================================================================== */

const Home = () => {
  return (
    <div className="content__wrapper">
      {" "}
      <News type="big" news={newsStub[0]} />
      <News type="vertical" news={newsStub[1]} />{" "}
      <News type="vertical" news={newsStub[2]} />{" "}
      <News type="vertical" news={newsStub[3]} />{" "}
      <News type="vertical" news={newsStub[4]} />{" "}
      <News type="horizontal" news={newsStub[5]} />{" "}
      <News type="horizontal" news={newsStub[6]} />{" "}
    </div>
  );
};

/* Add
  ========================================================================== */

const Add = props => {
  const fileInputRef = React.useRef(null);
  const [isCreateDisabled, setIsCreateDisabled] = React.useState(true);
  const [imageName, setImageName] = React.useState("");
  const [formData, setFormData] = React.useState({
    title: "",
    author: "",
    news: "",
    short: "",
    imageBase64: ""
  });

  React.useEffect(() => {
    validateForm(formData);
  }, [formData]);
  const handleImageSelectorClick = () => {
    fileInputRef.current.click();
  };

  const handleImageChange = event => {
    setImageName(event.target.files[0].name);
    setFormData({
      ...formData,
      imageBase64: event.target.files[0]
    });
  };

  const handleCancelClick = () => {
    props.isModalOpen(false);
  };

  const handleInputChange = event => {
    const input = event.target;
    const value = input.value;
    const name = input.name;

    setFormData({
      ...formData,
      [name]: value
    });

    renderFieldError(input);
  };

  const renderFieldError = (field) => {
    if (field.value.length === 0) {
      if (!field.classList.contains("add__input-styling--error")) {
        field.classList.add("add__input-styling--error");
      }
    } else {
      if (field.classList.contains("add__input-styling--error")) {
        field.classList.remove("add__input-styling--error");
      }
    }
  };

  const validateForm = form => {
    for (const key in form) {
      if (form[key] === "") {
        setIsCreateDisabled(true);
        return;
      }
    }

    setIsCreateDisabled(false);
  };

  const createNews = () => {
    console.log(formData);
  };

  return (
    <div className="modal__wrapper">
      <div className="add__wrapper">
        <form className="add__form">
          <section className="add__news-information">
            <div className="add__input-styling">
              <label className="add__input-label">Titulo</label>
              <input
                className="add__input asdasd"
                name="title"
                type="string"
                onChange={handleInputChange}
              />
            </div>
            <div className="add__input-styling">
              <label className="add__input-label">Autor</label>
              <input
                className="add__input"
                name="author"
                type="string"
                onChange={handleInputChange}
              />
            </div>
          </section>
          <div className="add__input-styling add__input-styling--big">
            <label className="add__input-label">Noticia</label>
            <textarea
              className="add__textarea"
              name="news"
              onChange={handleInputChange}
            />
          </div>
          <div className="add__image-tldr-wrapper">
            <div className="add__input-styling add__input-styling--small ">
              <label className="add__input-label ">Resumen</label>
              <textarea
                className="add__textarea"
                name="short"
                onChange={handleInputChange}
              />
            </div>
            <div className="add__image">
              {" "}
              <div
                className="add__input-styling"
                onClick={handleImageSelectorClick}
              >
                <label className="add__input-label">Agregar imagen</label>
                <input
                  className="add__file-input"
                  name="image"
                  type="file"
                  accept=".jpeg,.png"
                  ref={fileInputRef}
                  onChange={handleImageChange}
                />
                <p className="add__file-name"> {imageName} </p>
              </div>
            </div>
          </div>
          <div className="add_button-wrapper">
            <button
              type="button"
              className="add__button add__button--cancel"
              onClick={handleCancelClick}
            >
              {" "}
              Cancelar{" "}
            </button>
            <button
              type="button"
              className="add__button "
              onClick={() => {
                createNews();
                handleCancelClick();
              }}
              disabled={isCreateDisabled}
            >
              {" "}
              Crear{" "}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

Add.propTypes = {
    isModalOpen: PropTypes.function
  };
/* News
  ========================================================================== */

const News = props => {
  let history = useHistory();
  const getNewsComponent = (type, news) => {
    switch (type) {
      case "big":
        return (
          <div
            className={getWrapperClassName(type, news)}
            onClick={() => {
              history.push("/news/" + news.id);
            }}
          >
            {" "}
            <img className="news__image news__image--big" src={news.image} />
            <div className="news__text-wrapper news__text-wrapper--big">
              <h3 className="news__title news__title--big>"> {news.title}</h3>{" "}
              <div className="news__information news__information--big">
                <span> {news.date} </span>
                <span>
                  - <i>{news.author}</i>{" "}
                </span>
              </div>
              <article className="news__short news__short--big">
                {news.short}
              </article>
            </div>
          </div>
        );
      case "vertical":
        return (
          <div
            className={getWrapperClassName(type, news)}
            onClick={() => {
              history.push("/news/" + news.id);
            }}
          >
            {" "}
            <div className="news__text-wrapper news__text-wrapper--vertical">
              <h3 className="news__title news__title--vertical>">
                {" "}
                {news.title}
              </h3>{" "}
              <div className="news__information news__information--vertical">
                <span> {news.date} </span>
                <span>
                  - <i>{news.author}</i>{" "}
                </span>
              </div>
              <article className="news__short news__short--vertical">
                {news.short}
              </article>
            </div>
          </div>
        );
      case "horizontal":
        return (
          <div
            className={getWrapperClassName(type, news)}
            onClick={() => {
              history.push("/news/" + news.id);
            }}
          >
            {" "}
            <div className="news__text-wrapper news__text-wrapper--horizontal">
              <h3 className="news__title news__title--horizontal>">
                {" "}
                {news.title}
              </h3>{" "}
              <div className="news__information news__information--horizontal">
                <span> {news.date} </span>
                <span>
                  - <i>{news.author}</i>{" "}
                </span>
              </div>
            </div>
            <div className="news__image-wrapper news__image-wrapper--horizontal">
              <img
                className="news__image news__image--horizontal"
                src={news.image}
              />
            </div>
          </div>
        );
      default:
        break;
    }
  };
  /**
   * Devuelve el nombre de la clase dependiendo
   * de el tipo de noticia que se quiere mostrar.
   **/
  const getWrapperClassName = (type, news) => {
    return (
      `news__wrapper news__wrapper--${type}` +
      (news.seen === true ? " news__wrapper--seen" : "")
    );
  };

  return (
    <React.Fragment>{getNewsComponent(props.type, props.news)} </React.Fragment>
  );
};

News.propTypes = {
    type: PropTypes.string,
    news: {
        id: PropTypes.number,
        title: PropTypes.string,
        author: PropTypes.string,
        date: PropTypes.string,
        content: PropTypes.string,
        short: PropTypes.string,
        seen: PropTypes.bool
    }
  };
/* Article
  ========================================================================== */

const Article = () => {
  let { id } = useParams();
  const article = newsStub.filter(news => {
    return news.id === parseInt(id);
  })[0];
  const { title, author, image, content, date } = article;

  article.seen = true;

  return (
    <div className="article__wrapper">
      <h3 className="article__title"> {title} </h3>
      <p className="article__information">
        {" "}
        {date} - {author}
      </p>
      <img className="article__image" src={image} />

      <article className="article__content">{content} </article>
    </div>
  );
};

/* ==========================================================================
    APP
   ========================================================================== */

const App = () => (
  <div className="app">
    <Dashboard />
  </div>
);
ReactDOM.render(<App />, document.getElementById("root"));
